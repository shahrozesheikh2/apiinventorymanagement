import { ANY } from '../shared/common';
import { 
  spareParts,
  inventoryHistory
} from '.';

export * from './spareParts';
export * from './inventoryHistory';


type ModelType = ANY;

export const models: ModelType = [
  spareParts,
  inventoryHistory
]