import { AutoIncrement, BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table } from "sequelize-typescript";
import { spareParts } from "./spareParts";

export interface inventoryHistoryI {
    id: number;
    newPrice: number;
    sparePartId:number;
    newQuantity: number;
    returnQuantity: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
}

@Table({
    modelName: 'inventoryHistory',
    tableName: 'inventoryHistory',
    timestamps: true
})

export class inventoryHistory extends Model<inventoryHistoryI>{

    @BelongsTo((): typeof spareParts => spareParts)
    public spareParts : typeof spareParts;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.BIGINT)
    public newPrice: number;

    @Column(DataType.BIGINT)
    public newQuantity: number;

    @Column(DataType.BIGINT)
    public returnQuantity: number;

    @ForeignKey((): typeof spareParts  => spareParts )
    @Column(DataType.INTEGER)
    public sparePartId: number;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}

const getcondition = (type:any) => {
    if (type === 1) return "Mint"
    if (type === 2) return "Used"
    return ""
  };

const getCurrentState = (type:any) => {
    if (type === 1) return "available"
    if (type === 2) return "Low on stock"
    if (type === 3) return "unavailable"
    return ""
  };