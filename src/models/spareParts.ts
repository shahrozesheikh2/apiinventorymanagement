import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface sparePartsI {
    id: number;
    itemName: string;
    code: string;
    price: number;
    currentStateId:number;
    noOfused:number;
    fixFirstId:string;
    branId:number;
    productionYear:string;
    conditionId:number;
    companyId:number;
    notes:string;
    quantity: number;
    resources:JSON;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
}

@Table({
    modelName: 'spareParts',
    tableName: 'spareParts',
    timestamps: true
})

export class spareParts extends Model<sparePartsI>{

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    public id: number;

    @Column(DataType.TEXT)
    public itemName: string;

    @Column(DataType.TEXT)
    public code: string;

    @Column(DataType.BIGINT)
    public price: number;

    @Column(DataType.BIGINT)
    public quantity: number;

    @Column(DataType.INTEGER)
    public currentStateId: number;

    @Column({ type :DataType.VIRTUAL , 
        get() {return getCurrentState(this.getDataValue('currentStateId'));}})
        public currentState : string

    @Column(DataType.INTEGER)
    public noOfused: number;
  
    @Column(DataType.TEXT)
    public fixFirstId: string;

    @Column(DataType.INTEGER)
    public companyId : number;

    @Column(DataType.INTEGER)
    public branId: number;

    @Column(DataType.TEXT)
    public productionYear: string;

    @Column(DataType.INTEGER)
    public conditionId: number;

    @Column({ type :DataType.VIRTUAL , 
        get() {return getcondition(this.getDataValue('conditionId'));}})
        public condition : string

    @Column(DataType.TEXT)
    public notes: string;

    @Column(DataType.JSON)
    public resources:JSON;

    @Column(DataType.DATE)
    public createdAt: Date;

    @Column(DataType.INTEGER)
    public createdBy: number;

    @Column(DataType.DATE)
    public deletedAt: Date;

    @Column(DataType.INTEGER)
    public deletedBy: number;

    @Column(DataType.DATE)
    public updatedAt: Date;

    @Column(DataType.INTEGER)
    public updatedBy: number;

}

const getcondition = (type:any) => {
    if (type === 1) return "Mint"
    if (type === 2) return "Used"
    return ""
  };

const getCurrentState = (type:any) => {
    if (type === 1) return "available"
    if (type === 2) return "Low on stock"
    if (type === 3) return "unavailable"
    return ""
  };