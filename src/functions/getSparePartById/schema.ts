import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const sparePartSchema = Joi.object({ 
    id: Joi.number().integer().required()
})