import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { spareParts } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {sparePartSchema} from './schema';
import * as typings from '../../shared/common';

const getSparePartById: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
    try{
        sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
        sequelize.authenticate()

        const getAllScheme: typings.ANY = await sparePartSchema.validateAsync(event.body)
        const{
            id
        }= getAllScheme

        const sparePartResponse: typings.ANY = await spareParts.findOne({where:{id , deletedAt:null}, attributes: {exclude: ['createdBy', 'updatedBy']}})
        await sequelize.connectionManager.close();
        
        return formatJSONResponse({
            statusCode: 200,
            message: `Successfully Recieved`,
            sparePartResponse
        });
    }   
    catch(error) {
        await sequelize.connectionManager.close();
        return formatJSONResponse({
            statusCode: 403,
            message: error.message
        })
      }
}



export const main = middyfy(getSparePartById);