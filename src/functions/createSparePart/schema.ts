import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const sparePartSchema = Joi.object({ 
    branId: Joi.number().integer().required(),
    itemName:Joi.string().required(),
    productionYear:Joi.string().required(),
    conditionId:Joi.number().integer().required(),
    notes:Joi.string().required(),
    code:Joi.string().required(),
    price: Joi.number().integer().required(),
    quantity: Joi.number().integer().required(),
    companyId:Joi.number().integer().required(),
    userId:Joi.number().integer().required(),
    // currentStateId:Joi.number().integer().required(),// its depend upon stock available/ low on stock
    resources: Joi.array().items(Joi.object({
        awsKey: Joi.string(),
        awsUrl: Joi.string().uri(),
        description:Joi.string()
    }))
})