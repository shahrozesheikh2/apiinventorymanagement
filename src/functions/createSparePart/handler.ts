import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { spareParts, sparePartsI } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {sparePartSchema} from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';

const createSparePart: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
    try{
        sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
        sequelize.authenticate()

        const partData: typings.ANY = await sparePartSchema.validateAsync(event.body)
        const{
            code,
            companyId
        }= partData

        const uniqueValue = await spareParts.findOne({
            where:{ 
                code:{ 
                    [Op.like]: code
                },
                companyId
            }
        })

        if(uniqueValue){
            throw new Error('This item is already in the inventory');
        }

        const number  = parseInt(`${Math.random() * 1000000}`)

        const fixFirstId = `FF-${number}`

        let currentStateId //= 1 // available

        // if(partData.quantity <= 5 && partData.quantity > 0){
        //     currentStateId = 2 // low on stock
        // }else{
        //     currentStateId = 3
        // }

        if (partData.quantity<= 5 && partData.quantity > 0){
            currentStateId = 2
        } else if(partData.quantity> 5){
            currentStateId = 1
        } else{
            currentStateId = 3
        }

 
        const partObj = {
            ...partData,
            currentStateId,
            fixFirstId
        }

        console.log("partObj",partObj)

      

        const sparePartResponse: sparePartsI = await spareParts.create(partObj)
        await sequelize.connectionManager.close();
        
        return formatJSONResponse({
            statusCode: 200,
            message: `Successfully Created`,
            sparePartResponse
        });
    }   
    catch(error) {
        await sequelize.connectionManager.close();

        return formatJSONResponse({
            statusCode: 403,
            message: error.message
        })
      }
}



export const main = middyfy(createSparePart);