import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { spareParts } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {sparePartScheme} from './schema';
import * as typings from '../../shared/common';

const deleteSparePart: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
    try{
        sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
        sequelize.authenticate()

        const deleteSchema: typings.ANY = await sparePartScheme.validateAsync(event.body)
        const{
            id,
            userId
        }= deleteSchema

        const sparePartResponse: typings.ANY = await spareParts.update( { deletedBy: userId, deletedAt: new Date }, {where: {id}})
        
        await sequelize.connectionManager.close();
        
        return formatJSONResponse({
            statusCode: 200,
            message: `deleted spare part`,
            sparePartResponse
        });
    }   
    catch(error) {
        await sequelize.connectionManager.close();

        return formatJSONResponse({
            statusCode: 403,
            message: error.message
        })
      }
}



export const main = middyfy(deleteSparePart);