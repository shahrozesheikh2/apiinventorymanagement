import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const sparePartScheme = Joi.object({ 
    id:Joi.number().integer().required(),
    userId:Joi.number().integer().required(),
})