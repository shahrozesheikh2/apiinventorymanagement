import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const sparePartScheme = Joi.object({ 
    id:Joi.number().integer().required(),
    branId: Joi.number().integer(),
    itemName:Joi.string(),
    productionYear:Joi.string(),
    conditionId:Joi.number().integer(),
    notes:Joi.string(),
    code:Joi.string(),
    price: Joi.number().integer(),
    quantity: Joi.number().integer(),
    returnQuantity:Joi.number().integer(),
    companyId:Joi.number().integer(),
    resources: Joi.array().items(Joi.object({
        awsKey: Joi.string(),
        awsUrl: Joi.string().uri(),
        description:Joi.string()
    }))
})