import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { inventoryHistory, spareParts } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {sparePartScheme} from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';

const updateSparePart: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
    try{
        sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
        sequelize.authenticate()

        const updateSchema: typings.ANY = await sparePartScheme.validateAsync(event.body)
        const{
            id,
            code,
            quantity,
            returnQuantity,
            companyId,
            ...other
        }= updateSchema

        const productExist = await spareParts.findOne({ where:{ id } })

        if(!productExist){
            throw new Error('Spare Part does not exist');
        }

        const uniqueValue = await spareParts.findOne({
            where:{ 
                code: {[Op.like]: code },
                id:{[Op.ne]:id},
                companyId
            }
        })
       console.log("uniqueValue",uniqueValue)
        if(uniqueValue){
            throw new Error('This item Name is already in the inventory');
        }
        if(!other.price){
            other.price = productExist.price
        }

        let totalQuantity;
        if(quantity){
            const obj ={
                sparePartId :id,
                newQuantity: quantity,
                newPrice: other.price
            }
            await inventoryHistory.create(obj)
            totalQuantity = quantity + productExist.quantity
        }
        if(returnQuantity){
            const obj ={
                sparePartId :id,
                returnQuantity,
                newPrice: other.price
            }
            await inventoryHistory.create(obj)
            totalQuantity = returnQuantity + productExist.quantity
        }

        let currentStateId = productExist.currentStateId;

        console.log("totalQuantity",totalQuantity)

        if (totalQuantity<= 5 && totalQuantity > 0){
            currentStateId = 2
        } else if(totalQuantity> 5){
            currentStateId = 1
        } else{
            currentStateId = 3
        }
        console.log("currentStateId",currentStateId)
        console.log("other",other)

        const sparePartResponse: typings.ANY  = await spareParts.update({ ...other,currentStateId,quantity:totalQuantity, updatedAt: new Date }, {where: {id}})


        await sequelize.connectionManager.close();
        
        return formatJSONResponse({
            statusCode: 200,
            message: `SuccessFully Updated`,
            sparePartResponse
        });
    }   
    catch(error) {
        await sequelize.connectionManager.close();

        return formatJSONResponse({
            statusCode: 403,
          message: error.message
        
        })
      }
}



export const main = middyfy(updateSparePart);