export { default as hello } from './hello';
export { default as createSparePart } from './createSparePart'
export { default as updateSparePart } from './updateSparePart'
export { default as deleteSparePart } from './deleteSparePart'
export { default as getAllSparePart } from './getAllSparePart'
export { default as getSparePartById } from './getSparePartById'
export { default as getInventoryHistoryBySparePartId } from './getInventoryHistoryBySparePartId'
