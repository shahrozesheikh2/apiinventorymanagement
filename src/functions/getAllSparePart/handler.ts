import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { spareParts } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {sparePartScheme} from './schema';
import * as typings from '../../shared/common';
import { Op } from 'sequelize';
const getAllSparePart: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
    try{
        sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
        sequelize.authenticate()

        const getAllScheme: typings.ANY = await sparePartScheme.validateAsync(event.body)
        const{
            limit,
            offset,
            companyId,
            filters
        }= getAllScheme


        let  whereClause: { [key: string]: typings.ANY }  = {}

        if (companyId) {
            whereClause.companyId = companyId
        }
        
        if (filters?.itemName) {
            whereClause.itemName = {  [Op.startsWith]: filters.itemName };
        }
        if (filters?.code) {
            whereClause.code = {  [Op.startsWith]: filters.code };
        }
        if (filters?.fixFirstId) {
            whereClause.fixFirstId = {  [Op.startsWith]: filters.fixFirstId };
        }
          console.log("whereClause",whereClause)
        const sparePartResponse: typings.ANY = await spareParts.findAll({where:{deletedAt:null,...whereClause}, limit:limit,
            offset: offset * limit, attributes: {exclude: ['createdBy', 'updatedBy']}})
        
        await sequelize.connectionManager.close();
        
        return formatJSONResponse({
            statusCode: 200,
            message: `list spare part`,
            sparePartResponse
        });
    }   
    catch(error) {
        await sequelize.connectionManager.close();

        return formatJSONResponse({
            statusCode: 403,
          message: error.message
        })
      }
}



export const main = middyfy(getAllSparePart);