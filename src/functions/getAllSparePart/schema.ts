import * as Joi from 'joi'

export default {
    type: "object",
    properties: {}
} as const;

export const sparePartScheme = Joi.object({ 
    offset: Joi.number().min(0).default(0),
    limit: Joi.number().max(999).default(100),
    companyId:Joi.number().integer(),
    filters:Joi.object({
        itemName:Joi.string(),
        code:Joi.string(),
        fixFirstId:Joi.string()
      })
})