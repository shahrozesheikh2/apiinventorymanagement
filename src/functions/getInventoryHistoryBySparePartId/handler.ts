import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import { inventoryHistory, models, spareParts } from '../../models/index';
import { sequelize } from '../../config/database';
import schema, {sparePartSchema} from './schema';
import * as typings from '../../shared/common';

const getInventoryHistoryBySparePartId: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
    try{
        sequelize.connectionManager.initPools();

        // restore `getConnection()` if it has been overwritten by `close()`
        if (sequelize.connectionManager.hasOwnProperty("getConnection")) {
          delete sequelize.connectionManager.getConnection;
        }
        sequelize.authenticate()

        const getAllSchema: typings.ANY = await sparePartSchema.validateAsync(event.body)
        const{
            sparePartId
        }= getAllSchema

        const SparePartHistory: typings.ANY = await inventoryHistory.findAndCountAll({where:{sparePartId , deletedAt:null}, 
            include:{
                as:'spareParts',
                model: spareParts,
                attributes:  ['id', 'itemName','code','resources']
            },
            attributes: {exclude: ['createdBy', 'updatedBy']}})
        await sequelize.connectionManager.close();
        
        return formatJSONResponse({
            statusCode: 200,
            message: `Successfully Recieved`,
            SparePartHistory
        });
    }   
    catch(error) {
        await sequelize.connectionManager.close();
        return formatJSONResponse({
            statusCode: 403,
            message: error.message
        })
      }
}



export const main = middyfy(getInventoryHistoryBySparePartId);